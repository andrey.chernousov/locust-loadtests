from locust import HttpLocust, TaskSet, between, task
import ast
import uuid
import random

class FlowException(Exception):
    pass
IDMNEMOS_VIEW=[]
# IDMNEMOS_CREATEDELETE=[]
NAME_VIEW="ForDebug(Ch TestCafe V)"
# NAME_CREATEDELETE="ForDebug(Ch TestCafe D)"
# NAME="Ch1"

class NewAppApi(TaskSet):
    def __init__(self, parent):
        super(NewAppApi, self).__init__(parent)
        self._access_token=''

# class NewAppApi(TaskSet):
    def login(l):
        with l.client.post("http://192.168.101.91:80/auth/realms/master/protocol/openid-connect/token",
                      {"client_id": "portal", "grant_type": "password", "username": "admin", "password": "admin"}, name='Get token',
                      headers={"Connection": "close"}) as response:
            if response.status_code == 200:
                # print("200")
                # print(response.json())
                l._access_token=response.json().get('access_token')
            else:
                raise FlowException('Response code not 200')
        # print("def login(l)")

    def openMainPortal(l):
        l.client.get("/")
        l.client.get("/built/scripts/zone.min.js")
        l.client.get("/built/scripts/import-map-overrides.js")
        l.client.get("/built/scripts/system.min.js")
        l.client.get("/built/scripts/amd.min.js")
        l.client.get("/built/scripts/named-exports.js")
        l.client.get("/built/scripts/named-register.min.js")
        l.client.get("/built/vendors~main.js")
        l.client.get("/built/main.js")
        l.client.get("/built/assets/settings.json")
        l.client.get("/auth/realms/master/account")
        l.client.get("/json/applicationUnits")
        l.client.get("/built/scripts/single-spa.min.js")
        l.client.get("/json/UserSettings")
        l.client.get("/json/UserCommonApplications")

    def openEditorMnemosheme(l):
        l.client.get("/hostRootShell/assets/i18n/en_US.json")
        l.client.get("/hostMnemo/scripts.js")
        l.client.get("/mnemo/css/common.css")
        l.client.get("/mnemo/resources/graph.txt")
        l.client.get("/mnemo/resources/graph_ru.txt")
        l.client.get("/mnemo/resources/editor.txt")
        l.client.get("/mnemo/resources/editor_ru.txt")
        l.client.get("/hostMnemo/main.js")
        l.client.get("/hostMnemo/assets/i18n/en_US.json")
        l.client.get("/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json")
        l.client.get("/hostMnemo/assets/settings.json")
        l.client.get("/mnemo/resources/grapheditor.txt")
        l.client.get("/mnemo/styles/default.xml")
        l.client.get("/mnemo/stencils/mnemo.library.xml")

    def createOneMnemos(l, _name):
        _id=str(uuid.uuid1())
        _mnemos={
            "content": "<mxGraphModel dx=\"4180\" dy=\"1338\" grid=\"0\" gridSize=\"10\" guides=\"1\" tooltips=\"1\" connect=\"1\" arrows=\"1\" fold=\"1\" page=\"1\" pageScale=\"1\" pageWidth=\"1920\" pageHeight=\"1080\" background=\"#2C0059\"><root><mxCell id=\"0\"/><mxCell id=\"1\" parent=\"0\"/><mxCell id=\"40\" value=\"\" style=\"rounded=0;whiteSpace=wrap;html=1;fillColor=#7A737D;\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"-145\" y=\"67\" width=\"1776\" height=\"920\" as=\"geometry\"/></mxCell><mxCell id=\"26\" style=\"edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;strokeColor=#00FFFF;strokeWidth=4;entryX=0.436;entryY=0.073;entryDx=0;entryDy=0;entryPerimeter=0;\" parent=\"1\" source=\"21\" target=\"24\" edge=\"1\"><mxGeometry relative=\"1\" as=\"geometry\"><Array as=\"points\"><mxPoint x=\"835\" y=\"134\" as=\"0\"/><mxPoint x=\"1395\" y=\"134\" as=\"1\"/><mxPoint x=\"1395\" y=\"298\" as=\"2\"/><mxPoint x=\"1396\" y=\"298\" as=\"3\"/></Array><mxPoint x=\"1393\" y=\"283\" as=\"targetPoint\"/></mxGeometry></mxCell><mxCell id=\"21\" value=\"\" style=\"shape=mnemo.library.reservoir1;whiteSpace=wrap;html=0;fillColor=#808080;strokeColor=#000000;strokeWidth=1\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"763\" y=\"202.5\" width=\"140\" height=\"105\" as=\"geometry\"/></mxCell><mxCell id=\"22\" value=\"\" style=\"shape=mnemo.library.reservoir1;whiteSpace=wrap;html=0;fillColor=#808080;strokeColor=#000000;strokeWidth=1\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"781\" y=\"510\" width=\"140\" height=\"105\" as=\"geometry\"/></mxCell><mxCell id=\"24\" value=\"\" style=\"shape=mnemo.library.lorry;whiteSpace=wrap;html=0;fillColor=#808080;strokeColor=#000000;strokeWidth=1\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"1306\" y=\"307.5\" width=\"205\" height=\"97\" as=\"geometry\"/></mxCell><mxCell id=\"25\" value=\"\" style=\"shape=mnemo.library.tank;whiteSpace=wrap;html=0;fillColor=#808080;strokeColor=#000000;strokeWidth=1\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"1369\" y=\"704.5\" width=\"142\" height=\"93\" as=\"geometry\"/></mxCell><mxCell id=\"27\" style=\"edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;strokeColor=#00FF00;strokeWidth=4;exitX=0.503;exitY=0.027;exitDx=0;exitDy=0;exitPerimeter=0;\" parent=\"1\" source=\"22\" edge=\"1\"><mxGeometry relative=\"1\" as=\"geometry\"><mxPoint x=\"851\" y=\"463\" as=\"sourcePoint\"/><mxPoint x=\"1440\" y=\"703\" as=\"targetPoint\"/><Array as=\"points\"><mxPoint x=\"852\" y=\"452\" as=\"0\"/><mxPoint x=\"1440\" y=\"452\" as=\"1\"/><mxPoint x=\"1440\" y=\"708\" as=\"2\"/></Array></mxGeometry></mxCell><mxCell id=\"30\" style=\"edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;strokeColor=#00FFFF;strokeWidth=4;\" parent=\"1\" source=\"29\" edge=\"1\"><mxGeometry relative=\"1\" as=\"geometry\"><mxPoint x=\"771\" y=\"256\" as=\"targetPoint\"/></mxGeometry></mxCell><mxCell id=\"31\" style=\"edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;strokeColor=#00FF00;strokeWidth=4;\" parent=\"1\" source=\"29\" target=\"22\" edge=\"1\"><mxGeometry relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"29\" value=\"\" style=\"shape=mnemo.library.separator;whiteSpace=wrap;html=0;fillColor=#6262A3;strokeColor=#000000;strokeWidth=1\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"378\" y=\"255\" width=\"185\" height=\"360\" as=\"geometry\"/></mxCell><mxCell id=\"32\" value=\"50\" style=\"shape=mnemo.bar;rounded=0;whiteSpace=wrap;min=0;max=100\" parent=\"1\" vertex=\"1\" max=\"100\" min=\"0\"><mxGeometry x=\"874\" y=\"542\" width=\"35\" height=\"73\" as=\"geometry\"/><Array as=\"dataValue\"><Object pointType=\"1\" source=\"sinus50\" sourceType=\"1\" description=\"\" sourceId=\"70397\" typeDataSource=\"1\" as=\"0\"/></Array></mxCell><mxCell id=\"33\" value=\"50\" style=\"shape=mnemo.bar;rounded=0;whiteSpace=wrap;min=0;max=100\" parent=\"1\" vertex=\"1\" max=\"100\" min=\"0\"><mxGeometry x=\"851\" y=\"234.5\" width=\"35\" height=\"73\" as=\"geometry\"/><Array as=\"dataValue\"><Object pointType=\"1\" source=\"sinus25\" sourceType=\"1\" description=\"\" sourceId=\"70398\" typeDataSource=\"1\" as=\"0\"/></Array></mxCell><mxCell id=\"36\" value=\"&lt;div class=&quot;mnemo-chart area-spline&quot;&gt;&lt;/div&gt;\" style=\"shape=mnemo.splineChart;html=1;;fillColor=none;gradientColor=none;strokeColor=none;align=left;verticalAlign=top;\" parent=\"1\" vertex=\"1\" toNormolize=\"0\" isSeparateGraphBelow=\"1\" gridHorizontalShow=\"0\" gridVerticalShow=\"0\" legendShow=\"0\" legendPosition=\"1\" title=\"Расход сырья на колонну\"><mxGeometry x=\"-135\" y=\"255\" width=\"513\" height=\"352\" as=\"geometry\"/><Object from=\"*-1h\" to=\"*\" as=\"period\"/><Array as=\"dataValue\"><Object sourceId=\"70397\" source=\"sinus50\" description=\"\" pointType=\"1\" typeDataSource=\"1\" chartType=\"2\" showMarker=\"1\" markerShape=\"4\" lineSize=\"1\" color=\"#79f903\" as=\"0\"/></Array></mxCell><mxCell id=\"37\" value=\"&lt;div class=&quot;mnemo-chart pie&quot;&gt;&lt;/div&gt;\" style=\"shape=mnemo.pieChart;html=1;;fillColor=none;gradientColor=none;strokeColor=none;align=left;verticalAlign=top;\" parent=\"1\" vertex=\"1\" showValues=\"1\" showPercentage=\"1\" showLegend=\"0\" legendPosition=\"1\"><mxGeometry x=\"1002\" y=\"462.5\" width=\"266\" height=\"232\" as=\"geometry\"/><Array as=\"dataValue\"><Object sourceId=\"70397\" source=\"sinus50\" description=\"\" pointType=\"1\" typeDataSource=\"1\" color=\"#02ffff\" as=\"0\"/><Object sourceId=\"70398\" source=\"sinus25\" description=\"\" pointType=\"1\" typeDataSource=\"1\" color=\"#09fb10\" as=\"1\"/></Array></mxCell><mxCell id=\"42\" value=\"&lt;div class=&quot;mnemo-chart bubble&quot;&gt;&lt;/div&gt;\" style=\"shape=mnemo.bubbleChart;html=1;;fillColor=none;gradientColor=none;strokeColor=none;align=left;verticalAlign=top;\" parent=\"1\" vertex=\"1\" legendShow=\"0\" valuesShow=\"1\" legendPosition=\"1\"><mxGeometry x=\"921\" y=\"178\" width=\"379\" height=\"235\" as=\"geometry\"/><Object from=\"*-30m\" to=\"*\" as=\"period\"/><Array as=\"dataValue\"><Object sourceId=\"70397\" source=\"sinus50\" description=\"\" pointType=\"1\" typeDataSource=\"1\" color=\"#fff806\" valueColor=\"#000\" as=\"0\"/><Object sourceId=\"70398\" source=\"sinus25\" description=\"\" pointType=\"1\" typeDataSource=\"1\" color=\"#e70500\" valueColor=\"#000\" as=\"1\"/></Array></mxCell><mxCell id=\"43\" value=\"50\" style=\"shape=mnemo.gauge;rounded=0;whiteSpace=wrap;min=0;max=100\" parent=\"1\" vertex=\"1\" max=\"100\" min=\"0\"><mxGeometry x=\"1440\" y=\"172\" width=\"118\" height=\"114\" as=\"geometry\"/><Array as=\"dataValue\"><Object pointType=\"1\" source=\"sinus25\" sourceType=\"1\" description=\"\" sourceId=\"70398\" typeDataSource=\"1\" as=\"0\"/></Array><Array as=\"colorRange\"><Object from=\"0\" to=\"60\" color=\"rgba(240,255,0,0.2)\" as=\"0\"/><Object from=\"60\" to=\"90\" color=\"rgba(0,255,18,0.34)\" as=\"1\"/><Object from=\"90\" to=\"100\" color=\"#ff0000\" as=\"2\"/></Array></mxCell><mxCell id=\"44\" value=\"50\" style=\"shape=mnemo.gauge;rounded=0;whiteSpace=wrap;min=0;max=100\" parent=\"1\" vertex=\"1\" max=\"100\" min=\"0\"><mxGeometry x=\"1454\" y=\"586\" width=\"104\" height=\"99\" as=\"geometry\"/><Array as=\"dataValue\"><Object pointType=\"1\" source=\"sinus50\" sourceType=\"1\" description=\"\" sourceId=\"70397\" typeDataSource=\"1\" as=\"0\"/></Array></mxCell></root></mxGraphModel>",
            "createDate": 1573112748445,
            "description": "null",
            "id": _id,
            "name": _name,
            "ts": 1573112748445
        }
        with l.client.post("/mnemobase/api/v1/mnemo", name="Creating mnemosheme", json=_mnemos, headers={'Authorization':'Bearer '+l._access_token}) as response:
            _sC = response.content.decode("utf-8")
            # print("create "+_sC)

            if response.status_code == 200:
                idsMnemos=[]
                _sIdMnemos=response.content.decode("utf-8")
                idsMnemos.append(_sIdMnemos)
                # print("ответ сервера "+_sIdMnemos)
                # _collection=ast.literal_eval(_sCollection)
                # print(len(idMnemo))
            else:
                raise FlowException('Response code not 200')

    def saveMnemos(l):
        _mnemos={
            "content": "<mxGraphModel dx=\"4180\" dy=\"1338\" grid=\"0\" gridSize=\"10\" guides=\"1\" tooltips=\"1\" connect=\"1\" arrows=\"1\" fold=\"1\" page=\"1\" pageScale=\"1\" pageWidth=\"1920\" pageHeight=\"1080\" background=\"#2C0059\"><root><mxCell id=\"0\"/><mxCell id=\"1\" parent=\"0\"/><mxCell id=\"40\" value=\"\" style=\"rounded=0;whiteSpace=wrap;html=1;fillColor=#7A737D;\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"-145\" y=\"67\" width=\"1776\" height=\"920\" as=\"geometry\"/></mxCell><mxCell id=\"26\" style=\"edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;strokeColor=#00FFFF;strokeWidth=4;entryX=0.436;entryY=0.073;entryDx=0;entryDy=0;entryPerimeter=0;\" parent=\"1\" source=\"21\" target=\"24\" edge=\"1\"><mxGeometry relative=\"1\" as=\"geometry\"><Array as=\"points\"><mxPoint x=\"835\" y=\"134\" as=\"0\"/><mxPoint x=\"1395\" y=\"134\" as=\"1\"/><mxPoint x=\"1395\" y=\"298\" as=\"2\"/><mxPoint x=\"1396\" y=\"298\" as=\"3\"/></Array><mxPoint x=\"1393\" y=\"283\" as=\"targetPoint\"/></mxGeometry></mxCell><mxCell id=\"21\" value=\"\" style=\"shape=mnemo.library.reservoir1;whiteSpace=wrap;html=0;fillColor=#808080;strokeColor=#000000;strokeWidth=1\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"763\" y=\"202.5\" width=\"140\" height=\"105\" as=\"geometry\"/></mxCell><mxCell id=\"22\" value=\"\" style=\"shape=mnemo.library.reservoir1;whiteSpace=wrap;html=0;fillColor=#808080;strokeColor=#000000;strokeWidth=1\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"781\" y=\"510\" width=\"140\" height=\"105\" as=\"geometry\"/></mxCell><mxCell id=\"24\" value=\"\" style=\"shape=mnemo.library.lorry;whiteSpace=wrap;html=0;fillColor=#808080;strokeColor=#000000;strokeWidth=1\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"1306\" y=\"307.5\" width=\"205\" height=\"97\" as=\"geometry\"/></mxCell><mxCell id=\"25\" value=\"\" style=\"shape=mnemo.library.tank;whiteSpace=wrap;html=0;fillColor=#808080;strokeColor=#000000;strokeWidth=1\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"1369\" y=\"704.5\" width=\"142\" height=\"93\" as=\"geometry\"/></mxCell><mxCell id=\"27\" style=\"edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;strokeColor=#00FF00;strokeWidth=4;exitX=0.503;exitY=0.027;exitDx=0;exitDy=0;exitPerimeter=0;\" parent=\"1\" source=\"22\" edge=\"1\"><mxGeometry relative=\"1\" as=\"geometry\"><mxPoint x=\"851\" y=\"463\" as=\"sourcePoint\"/><mxPoint x=\"1440\" y=\"703\" as=\"targetPoint\"/><Array as=\"points\"><mxPoint x=\"852\" y=\"452\" as=\"0\"/><mxPoint x=\"1440\" y=\"452\" as=\"1\"/><mxPoint x=\"1440\" y=\"708\" as=\"2\"/></Array></mxGeometry></mxCell><mxCell id=\"30\" style=\"edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;strokeColor=#00FFFF;strokeWidth=4;\" parent=\"1\" source=\"29\" edge=\"1\"><mxGeometry relative=\"1\" as=\"geometry\"><mxPoint x=\"771\" y=\"256\" as=\"targetPoint\"/></mxGeometry></mxCell><mxCell id=\"31\" style=\"edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;strokeColor=#00FF00;strokeWidth=4;\" parent=\"1\" source=\"29\" target=\"22\" edge=\"1\"><mxGeometry relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"29\" value=\"\" style=\"shape=mnemo.library.separator;whiteSpace=wrap;html=0;fillColor=#6262A3;strokeColor=#000000;strokeWidth=1\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"378\" y=\"255\" width=\"185\" height=\"360\" as=\"geometry\"/></mxCell><mxCell id=\"32\" value=\"50\" style=\"shape=mnemo.bar;rounded=0;whiteSpace=wrap;min=0;max=100\" parent=\"1\" vertex=\"1\" max=\"100\" min=\"0\"><mxGeometry x=\"874\" y=\"542\" width=\"35\" height=\"73\" as=\"geometry\"/><Array as=\"dataValue\"><Object pointType=\"1\" source=\"sinus50\" sourceType=\"1\" description=\"\" sourceId=\"70397\" typeDataSource=\"1\" as=\"0\"/></Array></mxCell><mxCell id=\"33\" value=\"50\" style=\"shape=mnemo.bar;rounded=0;whiteSpace=wrap;min=0;max=100\" parent=\"1\" vertex=\"1\" max=\"100\" min=\"0\"><mxGeometry x=\"851\" y=\"234.5\" width=\"35\" height=\"73\" as=\"geometry\"/><Array as=\"dataValue\"><Object pointType=\"1\" source=\"sinus25\" sourceType=\"1\" description=\"\" sourceId=\"70398\" typeDataSource=\"1\" as=\"0\"/></Array></mxCell><mxCell id=\"36\" value=\"&lt;div class=&quot;mnemo-chart area-spline&quot;&gt;&lt;/div&gt;\" style=\"shape=mnemo.splineChart;html=1;;fillColor=none;gradientColor=none;strokeColor=none;align=left;verticalAlign=top;\" parent=\"1\" vertex=\"1\" toNormolize=\"0\" isSeparateGraphBelow=\"1\" gridHorizontalShow=\"0\" gridVerticalShow=\"0\" legendShow=\"0\" legendPosition=\"1\" title=\"Расход сырья на колонну\"><mxGeometry x=\"-135\" y=\"255\" width=\"513\" height=\"352\" as=\"geometry\"/><Object from=\"*-1h\" to=\"*\" as=\"period\"/><Array as=\"dataValue\"><Object sourceId=\"70397\" source=\"sinus50\" description=\"\" pointType=\"1\" typeDataSource=\"1\" chartType=\"2\" showMarker=\"1\" markerShape=\"4\" lineSize=\"1\" color=\"#79f903\" as=\"0\"/></Array></mxCell><mxCell id=\"37\" value=\"&lt;div class=&quot;mnemo-chart pie&quot;&gt;&lt;/div&gt;\" style=\"shape=mnemo.pieChart;html=1;;fillColor=none;gradientColor=none;strokeColor=none;align=left;verticalAlign=top;\" parent=\"1\" vertex=\"1\" showValues=\"1\" showPercentage=\"1\" showLegend=\"0\" legendPosition=\"1\"><mxGeometry x=\"1002\" y=\"462.5\" width=\"266\" height=\"232\" as=\"geometry\"/><Array as=\"dataValue\"><Object sourceId=\"70397\" source=\"sinus50\" description=\"\" pointType=\"1\" typeDataSource=\"1\" color=\"#02ffff\" as=\"0\"/><Object sourceId=\"70398\" source=\"sinus25\" description=\"\" pointType=\"1\" typeDataSource=\"1\" color=\"#09fb10\" as=\"1\"/></Array></mxCell><mxCell id=\"42\" value=\"&lt;div class=&quot;mnemo-chart bubble&quot;&gt;&lt;/div&gt;\" style=\"shape=mnemo.bubbleChart;html=1;;fillColor=none;gradientColor=none;strokeColor=none;align=left;verticalAlign=top;\" parent=\"1\" vertex=\"1\" legendShow=\"0\" valuesShow=\"1\" legendPosition=\"1\"><mxGeometry x=\"921\" y=\"178\" width=\"379\" height=\"235\" as=\"geometry\"/><Object from=\"*-30m\" to=\"*\" as=\"period\"/><Array as=\"dataValue\"><Object sourceId=\"70397\" source=\"sinus50\" description=\"\" pointType=\"1\" typeDataSource=\"1\" color=\"#fff806\" valueColor=\"#000\" as=\"0\"/><Object sourceId=\"70398\" source=\"sinus25\" description=\"\" pointType=\"1\" typeDataSource=\"1\" color=\"#e70500\" valueColor=\"#000\" as=\"1\"/></Array></mxCell><mxCell id=\"43\" value=\"50\" style=\"shape=mnemo.gauge;rounded=0;whiteSpace=wrap;min=0;max=100\" parent=\"1\" vertex=\"1\" max=\"100\" min=\"0\"><mxGeometry x=\"1440\" y=\"172\" width=\"118\" height=\"114\" as=\"geometry\"/><Array as=\"dataValue\"><Object pointType=\"1\" source=\"sinus25\" sourceType=\"1\" description=\"\" sourceId=\"70398\" typeDataSource=\"1\" as=\"0\"/></Array><Array as=\"colorRange\"><Object from=\"0\" to=\"60\" color=\"rgba(240,255,0,0.2)\" as=\"0\"/><Object from=\"60\" to=\"90\" color=\"rgba(0,255,18,0.34)\" as=\"1\"/><Object from=\"90\" to=\"100\" color=\"#ff0000\" as=\"2\"/></Array></mxCell><mxCell id=\"44\" value=\"50\" style=\"shape=mnemo.gauge;rounded=0;whiteSpace=wrap;min=0;max=100\" parent=\"1\" vertex=\"1\" max=\"100\" min=\"0\"><mxGeometry x=\"1454\" y=\"586\" width=\"104\" height=\"99\" as=\"geometry\"/><Array as=\"dataValue\"><Object pointType=\"1\" source=\"sinus50\" sourceType=\"1\" description=\"\" sourceId=\"70397\" typeDataSource=\"1\" as=\"0\"/></Array></mxCell></root></mxGraphModel>",
            "createDate": 1573112748445,
            "description": "null",
            "id": "c77c9621-a862-4809-b591-8f04bb279652",
            "name": "ForDebug(Ch TestCafe 0)",
            "ts": 1574685163503
        }
        with l.client.put("/mnemobase/api/v1/mnemo", json=_mnemos, headers={'Authorization':'Bearer '+l._access_token}) as response:
            _sC = response.content.decode("utf-8")
            print(_sC)

            if response.status_code != 200:
                raise FlowException('Response code not 200')
                # _sIdMnemos=response.content#.decode("utf-8")
                # print(_sIdMnemos)
                # _collection=ast.literal_eval(_sCollection)
                # print(len(idMnemo))
            # else:

    def getListMnemosheme(l, _name):
        with l.client.get("/mnemobase/api/v1/mnemo/info", headers={'Authorization':'Bearer '+l._access_token}) as response:
            if response.status_code == 200:
                _sCollection=response.content.decode("utf-8")
                _collection=ast.literal_eval(_sCollection)
                idsMnemos=[]
                # IDMNEMOS.clear()
                for i in _collection:
                    if i['name'] == _name:
                    # if i['name'] == 'ForTest(Ch SIBURIO 00)':
                    # if i['name'] == 'Delet(Ch)':
                            idsMnemos.append(i['id'])
                # global IDMNEMOS
                # IDMNEMOS=idsMnemos
                return idsMnemos
                # print(len(IDMNEMOS))
            else:
                _sCollection=response.content.decode("utf-8")
                print("******************"+_sCollection)
                raise FlowException('Response code not 200')

    def gettingMnemo(l, _collectionMnemos):
        if len(_collectionMnemos)!=0:
            _ii=random.choice(_collectionMnemos)
            with l.client.get("/mnemobase/api/v1/mnemo/"+_ii, name="Get mnemosheme", headers={'Authorization': 'Bearer ' + l._access_token}) as response:
                if response.status_code != 200:
                    print("Error getting mnemosheme"+ _ii)
                    raise FlowException('Response code not 200')

            l.client.get("/mnemo/resources/grapheditor_ru.txt", headers={'Authorization': 'Bearer ' + l._access_token})
            l.client.get("/mnemo/styles/default.xml", headers={'Authorization': 'Bearer ' + l._access_token})

    def deletingOneMnemos(l, _collectionMnemos):
        # print("есть что удалить?")
        if len(_collectionMnemos)<20:
            return
        _ii=random.choice(_collectionMnemos)
        # print(_ii)
        with l.client.delete("/mnemobase/api/v1/mnemo/"+_ii, name="Deleting mnemosheme", headers={'Authorization': 'Bearer ' + l._access_token}) as response:
            if response.status_code != 200:
                print("Error deleting mnemosheme" + _ii)
                raise FlowException('Response code not 200')
            else:
                print("Deleting success" + _ii)

                # print(response.content)
                # print("del "+i)

    def editorMnemos(l, _collectionMnemos, _name):
        if len(_collectionMnemos)<1:
            return
        _ii=random.choice(_collectionMnemos)
        # print(_ii)
        _mnemos={
            "content": "<mxGraphModel dx=\"4180\" dy=\"1338\" grid=\"0\" gridSize=\"10\" guides=\"1\" tooltips=\"1\" connect=\"1\" arrows=\"1\" fold=\"1\" page=\"1\" pageScale=\"1\" pageWidth=\"1920\" pageHeight=\"1080\" background=\"#2C0059\"><root><mxCell id=\"0\"/><mxCell id=\"1\" parent=\"0\"/><mxCell id=\"40\" value=\"\" style=\"rounded=0;whiteSpace=wrap;html=1;fillColor=#7A737D;\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"-145\" y=\"67\" width=\"1776\" height=\"920\" as=\"geometry\"/></mxCell><mxCell id=\"26\" style=\"edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;strokeColor=#00FFFF;strokeWidth=4;entryX=0.436;entryY=0.073;entryDx=0;entryDy=0;entryPerimeter=0;\" parent=\"1\" source=\"21\" target=\"24\" edge=\"1\"><mxGeometry relative=\"1\" as=\"geometry\"><Array as=\"points\"><mxPoint x=\"835\" y=\"134\" as=\"0\"/><mxPoint x=\"1395\" y=\"134\" as=\"1\"/><mxPoint x=\"1395\" y=\"298\" as=\"2\"/><mxPoint x=\"1396\" y=\"298\" as=\"3\"/></Array><mxPoint x=\"1393\" y=\"283\" as=\"targetPoint\"/></mxGeometry></mxCell><mxCell id=\"21\" value=\"\" style=\"shape=mnemo.library.reservoir1;whiteSpace=wrap;html=0;fillColor=#808080;strokeColor=#000000;strokeWidth=1\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"763\" y=\"202.5\" width=\"140\" height=\"105\" as=\"geometry\"/></mxCell><mxCell id=\"22\" value=\"\" style=\"shape=mnemo.library.reservoir1;whiteSpace=wrap;html=0;fillColor=#808080;strokeColor=#000000;strokeWidth=1\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"781\" y=\"510\" width=\"140\" height=\"105\" as=\"geometry\"/></mxCell><mxCell id=\"24\" value=\"\" style=\"shape=mnemo.library.lorry;whiteSpace=wrap;html=0;fillColor=#808080;strokeColor=#000000;strokeWidth=1\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"1306\" y=\"307.5\" width=\"205\" height=\"97\" as=\"geometry\"/></mxCell><mxCell id=\"25\" value=\"\" style=\"shape=mnemo.library.tank;whiteSpace=wrap;html=0;fillColor=#808080;strokeColor=#000000;strokeWidth=1\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"1369\" y=\"704.5\" width=\"142\" height=\"93\" as=\"geometry\"/></mxCell><mxCell id=\"27\" style=\"edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;strokeColor=#00FF00;strokeWidth=4;exitX=0.503;exitY=0.027;exitDx=0;exitDy=0;exitPerimeter=0;\" parent=\"1\" source=\"22\" edge=\"1\"><mxGeometry relative=\"1\" as=\"geometry\"><mxPoint x=\"851\" y=\"463\" as=\"sourcePoint\"/><mxPoint x=\"1440\" y=\"703\" as=\"targetPoint\"/><Array as=\"points\"><mxPoint x=\"852\" y=\"452\" as=\"0\"/><mxPoint x=\"1440\" y=\"452\" as=\"1\"/><mxPoint x=\"1440\" y=\"708\" as=\"2\"/></Array></mxGeometry></mxCell><mxCell id=\"30\" style=\"edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;strokeColor=#00FFFF;strokeWidth=4;\" parent=\"1\" source=\"29\" edge=\"1\"><mxGeometry relative=\"1\" as=\"geometry\"><mxPoint x=\"771\" y=\"256\" as=\"targetPoint\"/></mxGeometry></mxCell><mxCell id=\"31\" style=\"edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;strokeColor=#00FF00;strokeWidth=4;\" parent=\"1\" source=\"29\" target=\"22\" edge=\"1\"><mxGeometry relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"29\" value=\"\" style=\"shape=mnemo.library.separator;whiteSpace=wrap;html=0;fillColor=#6262A3;strokeColor=#000000;strokeWidth=1\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"378\" y=\"255\" width=\"185\" height=\"360\" as=\"geometry\"/></mxCell><mxCell id=\"32\" value=\"50\" style=\"shape=mnemo.bar;rounded=0;whiteSpace=wrap;min=0;max=100\" parent=\"1\" vertex=\"1\" max=\"100\" min=\"0\"><mxGeometry x=\"874\" y=\"542\" width=\"35\" height=\"73\" as=\"geometry\"/><Array as=\"dataValue\"><Object pointType=\"1\" source=\"sinus50\" sourceType=\"1\" description=\"\" sourceId=\"70397\" typeDataSource=\"1\" as=\"0\"/></Array></mxCell><mxCell id=\"33\" value=\"50\" style=\"shape=mnemo.bar;rounded=0;whiteSpace=wrap;min=0;max=100\" parent=\"1\" vertex=\"1\" max=\"100\" min=\"0\"><mxGeometry x=\"851\" y=\"234.5\" width=\"35\" height=\"73\" as=\"geometry\"/><Array as=\"dataValue\"><Object pointType=\"1\" source=\"sinus25\" sourceType=\"1\" description=\"\" sourceId=\"70398\" typeDataSource=\"1\" as=\"0\"/></Array></mxCell><mxCell id=\"36\" value=\"&lt;div class=&quot;mnemo-chart area-spline&quot;&gt;&lt;/div&gt;\" style=\"shape=mnemo.splineChart;html=1;;fillColor=none;gradientColor=none;strokeColor=none;align=left;verticalAlign=top;\" parent=\"1\" vertex=\"1\" toNormolize=\"0\" isSeparateGraphBelow=\"1\" gridHorizontalShow=\"0\" gridVerticalShow=\"0\" legendShow=\"0\" legendPosition=\"1\" title=\"Расход сырья на колонну\"><mxGeometry x=\"-135\" y=\"255\" width=\"513\" height=\"352\" as=\"geometry\"/><Object from=\"*-1h\" to=\"*\" as=\"period\"/><Array as=\"dataValue\"><Object sourceId=\"70397\" source=\"sinus50\" description=\"\" pointType=\"1\" typeDataSource=\"1\" chartType=\"2\" showMarker=\"1\" markerShape=\"4\" lineSize=\"1\" color=\"#79f903\" as=\"0\"/></Array></mxCell><mxCell id=\"37\" value=\"&lt;div class=&quot;mnemo-chart pie&quot;&gt;&lt;/div&gt;\" style=\"shape=mnemo.pieChart;html=1;;fillColor=none;gradientColor=none;strokeColor=none;align=left;verticalAlign=top;\" parent=\"1\" vertex=\"1\" showValues=\"1\" showPercentage=\"1\" showLegend=\"0\" legendPosition=\"1\"><mxGeometry x=\"1002\" y=\"462.5\" width=\"266\" height=\"232\" as=\"geometry\"/><Array as=\"dataValue\"><Object sourceId=\"70397\" source=\"sinus50\" description=\"\" pointType=\"1\" typeDataSource=\"1\" color=\"#02ffff\" as=\"0\"/><Object sourceId=\"70398\" source=\"sinus25\" description=\"\" pointType=\"1\" typeDataSource=\"1\" color=\"#09fb10\" as=\"1\"/></Array></mxCell><mxCell id=\"42\" value=\"&lt;div class=&quot;mnemo-chart bubble&quot;&gt;&lt;/div&gt;\" style=\"shape=mnemo.bubbleChart;html=1;;fillColor=none;gradientColor=none;strokeColor=none;align=left;verticalAlign=top;\" parent=\"1\" vertex=\"1\" legendShow=\"0\" valuesShow=\"1\" legendPosition=\"1\"><mxGeometry x=\"921\" y=\"178\" width=\"379\" height=\"235\" as=\"geometry\"/><Object from=\"*-30m\" to=\"*\" as=\"period\"/><Array as=\"dataValue\"><Object sourceId=\"70397\" source=\"sinus50\" description=\"\" pointType=\"1\" typeDataSource=\"1\" color=\"#fff806\" valueColor=\"#000\" as=\"0\"/><Object sourceId=\"70398\" source=\"sinus25\" description=\"\" pointType=\"1\" typeDataSource=\"1\" color=\"#e70500\" valueColor=\"#000\" as=\"1\"/></Array></mxCell><mxCell id=\"43\" value=\"50\" style=\"shape=mnemo.gauge;rounded=0;whiteSpace=wrap;min=0;max=100\" parent=\"1\" vertex=\"1\" max=\"100\" min=\"0\"><mxGeometry x=\"1440\" y=\"172\" width=\"118\" height=\"114\" as=\"geometry\"/><Array as=\"dataValue\"><Object pointType=\"1\" source=\"sinus25\" sourceType=\"1\" description=\"\" sourceId=\"70398\" typeDataSource=\"1\" as=\"0\"/></Array><Array as=\"colorRange\"><Object from=\"0\" to=\"60\" color=\"rgba(240,255,0,0.2)\" as=\"0\"/><Object from=\"60\" to=\"90\" color=\"rgba(0,255,18,0.34)\" as=\"1\"/><Object from=\"90\" to=\"100\" color=\"#ff0000\" as=\"2\"/></Array></mxCell><mxCell id=\"44\" value=\"50\" style=\"shape=mnemo.gauge;rounded=0;whiteSpace=wrap;min=0;max=100\" parent=\"1\" vertex=\"1\" max=\"100\" min=\"0\"><mxGeometry x=\"1454\" y=\"586\" width=\"104\" height=\"99\" as=\"geometry\"/><Array as=\"dataValue\"><Object pointType=\"1\" source=\"sinus50\" sourceType=\"1\" description=\"\" sourceId=\"70397\" typeDataSource=\"1\" as=\"0\"/></Array></mxCell></root></mxGraphModel>",
            "createDate": 1573112748445,
            "description": "null",
            "id": _ii,
            "name": _name,
            "ts": 1573112748445
        }
        with l.client.put("/mnemobase/api/v1/mnemo", json=_mnemos,
                           headers={'Authorization': 'Bearer ' + l._access_token}) as response:
            _sC = response.content.decode("utf-8")
            if response.status_code != 200:
                raise FlowException('Response code not 200')

    def deletingAllMnemos(l, _collectionMnemos):
        print("Deleting all: "+str(len(_collectionMnemos)))
        for i in _collectionMnemos:
            with l.client.delete("/mnemobase/api/v1/mnemo/"+i, name="Deleting all mnemosheme", headers={'Authorization': 'Bearer ' + l._access_token}) as response:
                if response.status_code == 200:
                    print(response.content)
                    # print("del "+i)
                else:
                    raise FlowException('Response code not 200')
        # print("del all")




class ViewUser(NewAppApi):
    def setup(self):
        print("Create 2 mnemos view")
        self.login()
        self.createOneMnemos(NAME_VIEW)
        self.createOneMnemos(NAME_VIEW)

    def on_start(self):
        self.login()
        print("***************on_start ViewUser logining")

    @task(10)
    def openEditor(self):
        self.openMainPortal()
        self.openEditorMnemosheme()
        global IDMNEMOS_VIEW
        IDMNEMOS_VIEW=self.getListMnemosheme(NAME_VIEW)
        self.gettingMnemo(IDMNEMOS_VIEW)
        # self.index()

    def on_stop(self):
        print("one stop")

    def teardown(self):
        print("All sheme deleted")
        self.login()
        global IDMNEMOS_VIEW
        IDMNEMOS_VIEW=self.getListMnemosheme(NAME_VIEW)
        self.deletingAllMnemos(IDMNEMOS_VIEW)

# class CreateShemeUser(NewAppApi):
#     def setup(self):
#         print("Create 2 mnemos")
#         self.login()
#         self.createOneMnemos(NAME_CREATEDELETE)
#         self.createOneMnemos(NAME_CREATEDELETE)
#
#     def on_start(self):
#         self.login()
#         print("on_start CreateShemeUser logining")
#
#     @task(100)
#     def openEditor(self):
#         self.openMainPortal()
#         self.openEditorMnemosheme()
#         global IDMNEMOS_CREATEDELETE
#         IDMNEMOS_CREATEDELETE = self.getListMnemosheme(NAME_CREATEDELETE)
#         self.gettingMnemo(IDMNEMOS_CREATEDELETE)
#
#     @task(10)
#     def addOneMnemos(self):
#         self.openMainPortal()
#         self.openEditorMnemosheme()
#         global IDMNEMOS_CREATEDELETE
#         IDMNEMOS_CREATEDELETE = self.getListMnemosheme(NAME_CREATEDELETE)
#         self.gettingMnemo(IDMNEMOS_CREATEDELETE)
#         print("Размер до добавления: "+str(len(IDMNEMOS_CREATEDELETE)))
#         self.createOneMnemos(NAME_CREATEDELETE)
#         IDMNEMOS_CREATEDELETE = self.getListMnemosheme(NAME_CREATEDELETE)
#         print("Размер после добавления: "+str(len(IDMNEMOS_CREATEDELETE)))
#
#     @task(1)
#     def dltOneMnemos(self):
#         self.openMainPortal()
#         self.openEditorMnemosheme()
#         global IDMNEMOS_CREATEDELETE
#         IDMNEMOS_CREATEDELETE = self.getListMnemosheme(NAME_CREATEDELETE)
#         print("Размер до удаления: "+str(len(IDMNEMOS_CREATEDELETE)))
#         self.deletingOneMnemos(IDMNEMOS_CREATEDELETE)
#         IDMNEMOS_CREATEDELETE = self.getListMnemosheme(NAME_CREATEDELETE)
#         print("Размер после удаления: "+str(len(IDMNEMOS_CREATEDELETE)))
#
#     def teardown(self):
#         print("All sheme deleted")
#         self.login()
#         global IDMNEMOS_CREATEDELETE
#         IDMNEMOS_CREATEDELETE = self.getListMnemosheme(NAME_CREATEDELETE)
#         self.deletingAllMnemos(IDMNEMOS_CREATEDELETE)

class EditedShemeUser(NewAppApi):
    def on_start(self):
        self.login()
        print("on_start EditedShemeUser logining")

    @task(5)
    def openEditor(self):
        self.openMainPortal()
        self.openEditorMnemosheme()
        global IDMNEMOS_VIEW
        IDMNEMOS_VIEW=self.getListMnemosheme(NAME_VIEW)
        self.gettingMnemo(IDMNEMOS_VIEW)

    @task(5)
    def edtOneMnemos(self):
        self.openMainPortal()
        self.openEditorMnemosheme()
        global IDMNEMOS_VIEW
        IDMNEMOS_VIEW=self.getListMnemosheme(NAME_VIEW)
        self.editorMnemos(IDMNEMOS_VIEW, NAME_VIEW)


class ViewSheme_User(HttpLocust):
    task_set = ViewUser
    weight = 1
    wait_time = between(0.200, 0.400)

# class CreateSheme_User(HttpLocust):
#     task_set = CreateShemeUser
#     weight = 1
#     wait_time = between(0.200, 0.400)

class EditedSheme_User(HttpLocust):
    task_set = EditedShemeUser
    weight = 1
    wait_time = between(0.200, 0.400)






